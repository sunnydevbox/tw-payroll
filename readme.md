# Payroll Package

## Requirements
1. PHP 7.1 or higher
2. Mysql 5.x
3. PHP Mcrypt
4. PHP Mysql
5. PHP 
6. Laravel 5.4.x

## Installation
1) Install and configure a fresh instance of Laravel 5.4.x

   `composer create-project laravel/laravel="5.4.*" project-name`

2) composer.json: Include this in the repositories section
```javascript
"repositories": [
    ...
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-core.git"
        },
        {
            "type": "git",
            "url": "https://bitbucket.org/sunnydevbox/tw-user.git"
        }
        ...
    ]
```

3) Composer: Require the package
```sh
composer require sunnydevbox/united-rebuilders-inc-payroll:dev-master
```

4) Update config/app.php. Add these under 'providers'
```ssh
Barryvdh\Cors\ServiceProvider::class,
Sunnydevbox\CebuUnitedRebuilders\CebuUnitedRebuildersServiceProvider::class,
```

5) Update 
```ssh
composer update
```

## Installation

1) Manage migrations
```sh
$ php artisan cebuunitedrebuilders:migrate --action[run/reset/refresh/rollback]
```
2) Clear all cache
```sh
$ php artisan twcore:optimze
```
3) Configure .env
```javascript
API_NAME="Cebu United Rebuilders Inc. API"
API_STRICT=true
API_VERSION=v1
API_PREFIX=api
API_DEFAULT_FORMAT=json
API_DEBUG=true
API_SUBTYPE=cebuunitedrebuilders
```

## Getting Started
1) Setup core package
```ssh
> php artisan twcore:publish-migrate
> php artisan twcore:publish-migrations
> php artisan twcore:publish-config
> php artisan twcore:optimize
```

2) Setup users package
```ssh
> php artisan twuser:publish-migrations
> php artisan twuser:publish-config
> php artisan twcore:optimize
```
3) Setup package
```ssh
> php artisan cebuunitedrebuilders:publish-migrations
> php artisan cebuunitedrebuilders:migrations --action=run
> php artisan twcore:optimize
```

## Server Configuration
1. Nginx
2. PHP-FPM 7.1+

## Queue Configuration
Coming soon