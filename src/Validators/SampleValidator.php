<?php

namespace Sunnydevbox\TWPayroll\Validators;

use \Prettus\Validator\Contracts\ValidatorInterface;
use \Prettus\Validator\LaravelValidator;

class SampleValidator extends LaravelValidator {

    protected $rules = [
        ValidatorInterface::RULE_CREATE => [
            'email'         => 'required|email',
            'password'      => 'min:8',
        ],
        ValidatorInterface::RULE_UPDATE => [
            'email'         => 'email',
            'password'      => 'min:8',
        ]
   ];

}