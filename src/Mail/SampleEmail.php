<?php

namespace Sunnydevbox\TWPayroll\Mail;
//namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Sunnydevbox\TWPayroll\Models\User;

class SampleEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $user;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // resources/email/verified.blade.php
        return $this->view('tw-payroll::mail.verified')
                    ->with([
                        'name' => $this->user->getMeta('first_name') . ' ' . $this->user->getMeta('last_name'),
                    ])
                    ->subject('RecoverHub :: Welcome to RecoveryHub!')
                    //->from()
                    ->to($this->user->email, $this->user->first_name);
    }
}
