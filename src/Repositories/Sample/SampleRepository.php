<?php 
namespace Sunnydevbox\TWInventory\Repositories\Category;

use Sunnydevbox\TWCore\Repositories\TWBaseRepository;

class SampleRepository extends TWBaseRepository
{

    //all, paginate, find, findByField, findWhere, getByCriteria
    //protected $cacheOnly = ['all', 'paginate'];
    protected $cacheExcept = [];

    /**
     * Specify Validator class name
     *
     * @return mixed
     */
    public function validator()
    {
        return "\Sunnydevbox\TWInventory\Validators\SampleValidator";
    }

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return '\Sunnydevbox\TWInventory\Models\Sample';
    }
}

