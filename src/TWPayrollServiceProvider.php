<?php

namespace Sunnydevbox\TWPayroll;

use Sunnydevbox\TWCore\BaseServiceProvider;

class TWPayrollServiceProvider extends BaseServiceProvider
{
    public function mergeConfig()
    {
        return [
           realpath(__DIR__ . '/../config/config.php') => 'TWPayroll'
        ];
    }

    public function loadRoutes()
    {
        return [
            realpath(__DIR__.'/../routes/api.php')
        ];
    }

    public function loadViews()
    {
        return [
            __DIR__.'/../resources/views' => 'TWPayroll',
        ];
    }
    
    public function registerProviders()
    {
        // EVENTS
        if (class_exists('\Sunnydevbox\TWPayroll\EventServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWPayroll\EventServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWPayroll\EventServiceProvider::class);    
        }

        if (class_exists('\Sunnydevbox\TWPim\TWPimServiceProvider')
            && !$this->app->resolved('\Sunnydevbox\TWPim\TWPimServiceProvider')
        ) {
            $this->app->register(\Sunnydevbox\TWPim\TWPimServiceProvider::class);    
        }
    }

    public function registerCommands()
    {
        if ($this->app->runningInConsole()) {

            $this->commands([
                \Sunnydevbox\TWPayroll\Console\Commands\PublishConfigCommand::class,
                \Sunnydevbox\TWPayroll\Console\Commands\MigrateCommand::class,
            ]);
        }
    }
}